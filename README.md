This doesn't even need a readme file.

# Sample Output:

Example with this gist: http://www.keyboard-layout-editor.com/#/gists/3aede80592346f947cbc1eb91574d1c7

    ./index.js 3aede80592346f947cbc1eb91574d1c7

```c
/*
    [LOCK+] [ESC  ] [1!   ] [2@   ] [3#   ] [4$   ] [5%   ] [Macro] [6^   ] [7&   ] [8*   ] [9(   ] [0)   ] [ESC  ] [LOCK+]
    [FN   ] [OS   ] [Q    ] [W    ] [E    ] [R    ] [T    ] [M1   ] [Y    ] [U    ] [I    ] [O    ] [P    ] [OS   ] [FN   ]
    [L3   ] [LALT ] [A    ] [S    ] [D    ] [F    ] [G    ] [M2   ] [H    ] [J    ] [K    ] [L    ] [:;   ] [RALT ] [L3   ]
    [L2   ] [/CAPS] [Z    ] [X    ] [C    ] [V    ] [B/<i ] [M3   ] [N    ] [M    ] [\ | /] [↑    ] [.,   ] [/CAPS] [L2   ]
    [SWAP ] [CTRL ] [/Acce] [/Smil] [MENU ] [SPACE] [SPACE] [ENTER] [SPACE] [SPACE] [←    ] [↓    ] [→    ] [RCTRL] [SWAP ]
*/
```
