#!/usr/bin/node
const { main, formatOutputArray } = require('./lib')

function usage() {
	console.log('Usage: ./generate-ascii-comment-keymap-from-kle-layout GISTID-OR-URL [>> tempfile.h]')
}

if (process.args<2)
	return usage()

const gistIdOrUrl = process.argv[2]
if (!gistIdOrUrl) {
	return usage()
}

main(gistIdOrUrl, (err, output2dArray, metadata) => {
	formatOutputArray(output2dArray, metadata)
})