const {get} = require('gist-get')
const fs = require('fs')
const mymodule = {}

mymodule.getJsonFromGist = function(gistId, cb) {

	//Fetch gist (json with all files)
	get(gistId, (err, gistData) => {

		if (err) {
			return cb(err)
		}

		//Take the first JSON in files, and try parse it
		let k = null
		for (k in gistData.files) {

			const file = gistData.files[k]

			//Skip if not json
			if (file.type !== 'application/json')
				continue

			try {
				const jsonObj = JSON.parse(file.content)
				//Debug save for sample json
				/*eslint no-sync: "off"*/
				//fs.writeFileSync('./' + file.filename, file.content)
				return cb(false, jsonObj)
			}
			catch (jsonParseErr) {
				return cb(jsonParseErr)
			}
		}

		//No jsons were found
		return cb(new Error('no json file found.'))
	})
}

mymodule.build1dArray = function(inputJsonObj, cb) {

	const input1dArray = []
	let maxC=0
	let r=0

	let keyboardMetaData = null

	//Parse input file lines
	inputJsonObj.map((row) => {

		//Is keyboard metadata (first row, optionnal)
		if (!Array.isArray(row)) {
			keyboardMetaData = row
			return null
		}

		let c=0
		let lastRowMetadata = null
		for (const k in row) {

			const col = row[k]
			let currentRowMetadata = null

			if (typeof(col)!=='string') {
				currentRowMetadata = col
				//Persistant or next key metadata?
				//Needs merge ?
				lastRowMetadata = currentRowMetadata
				continue
			}

			//Item with W specified (width = 1 item per width in the output row)
			if (lastRowMetadata) {

				if (lastRowMetadata.w) {
					//c=w;
					for (let c=0;c<lastRowMetadata.w;c++) {
						const label = (col==='' ? ' ' : col).replace(/[\n]+/g, '/')
						input1dArray.push({
							x: r,
							y: c,
							label: (c===0 ? label :' ')
						})
					}
					if (lastRowMetadata.w>maxC)
						maxC=lastRowMetadata.w
					lastRowMetadata = null
					continue
				}

				if (lastRowMetadata.x) {
					//Skip lines with negative X
					if (lastRowMetadata.x <= -1) {
						lastRowMetadata = null
						continue
					}
					//Handle X offset
					c+=lastRowMetadata.x
				}
			}

			lastRowMetadata = null
			const label = (col==='' ? ' ' : col).replace(/[\n]+/g, '/')

			input1dArray.push({
				y: r,
				x: c,
				label
			})
			c++
			if (c>maxC)
				maxC=c
		}
		r++
		return null
	})

	return cb(false, input1dArray, r, maxC, keyboardMetaData)
}

mymodule.build2dArray = function(input1dArray, r, c, metadata, cb) {

	//console.log("//Array dimensions: " + r + "x" + c);

	const newMetaData = {
		kbdproperties: {},
		dimensions: {x: c, y: r}
	}

	//Make an empty 2D array for puttings keys in
	const output2dArray = new Array(r)
	for (let y=0;y<r;y++)
		output2dArray[y] = new Array(c)

	//Put everything back in place in the new 2d array
	input1dArray.map((item) => {
		/*
        An item look like this:
            {
                x:      ...,
                y:      ...,
                label:  ...
            }
        */
		output2dArray[item.y][item.x] = item.label
		return null
	})

	//Fill emptys
	/*
    output2dArray = output2dArray.map(function(row){
        return row.map(function(col) {
            return (typeof(col)=='string' ? col : ' ');
        });
    })
    */

	return cb(false, output2dArray, newMetaData)
}

mymodule.formatOutputArray = function(output2dArray, metadata) {
	for (let y=0;y<metadata.dimensions.y;y++) {
		for (let x=0;x<metadata.dimensions.x;x++) {
			const col = output2dArray[y][x]
			const label = col ? col.substr(0,5) : ' '
			process.stdout.write('[' + (label.length < 5 ? label + ' '.repeat(5-label.length) : label.substr(0,5)) + ']' +  '\t')
		}
		process.stdout.write('\n')
	}
}

mymodule.main = function(gistId, cb) {

	//Get one layout obj file from gist's files
	mymodule.getJsonFromGist(gistId, (err, jsonObj) => {
		if (err) {
			console.error('Error getting Json from gist:' )
			console.error(err)
			return
		}
		mymodule.build1dArray(jsonObj, (err, input1dArray, r, c, metadata) => {
			if (err) {
				console.error('Error building 1d array:' )
				console.error(err)
				return
			}
			mymodule.build2dArray(input1dArray, r, c, metadata, (err, output2dArray, metadata) => {
				if (err) {
					console.error('Error building 2d array:')
					console.error(err)
					return
				}
				return cb(err, output2dArray, metadata)
			})

		})
	})
}

module.exports =  mymodule
