const { main, formatOutputArray } = require('./lib')
const async = require('async')

const layers = [
	'3aede80592346f947cbc1eb91574d1c7',
	'873f4eb3c290906364daf000cb132b69',
	'6ee8724672840abd6863985fa9fe7014',
	'ea5fd62e5090bfd1ace06bb00ebc35b5'
]

async.mapSeries(layers, (layer, cb) => {

	console.log()
	console.log('//KLE url: http://www.keyboard-layout-editor.com/#/gists/' + layer)
	console.log('/*')

	main(layer, (err, output2dArray, metadata) => {

		formatOutputArray(output2dArray, metadata)

		console.log('*/')
		console.log()

		return cb(false)
	})

}, () => {

	console.log()
})
